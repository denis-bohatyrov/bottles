import 'dart:io';

import 'package:bottles/bottles_localizations.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:image_cropper/image_cropper.dart';

class ImagePickerWidget extends StatefulWidget {
  final Widget child;
  final ValueChanged<File> onImageSelected;
  final bool useInkWell;
  final double ratioX;
  final double ratioY;
  final int maxWidth;
  final int maxHeight;
  final bool circleShape;

  ImagePickerWidget({
    Key key,
    @required this.child,
    @required this.onImageSelected,
    this.useInkWell = true,
    this.ratioX,
    this.ratioY,
    this.maxWidth,
    this.maxHeight,
    this.circleShape,
  })  : assert(child != null),
        assert(onImageSelected != null),
        super(key: key);

  @override
  _ImagePickerWidgetState createState() => _ImagePickerWidgetState();
}

class _ImagePickerWidgetState extends State<ImagePickerWidget> {
  Future<void> _showSelectionMenu() async {
    final source = await showModalBottomSheet(
      context: context,
      builder: _buildBottomMenu,
    );
    if (source != null) {
      await _selectPhoto(source);
    }
  }

  Widget _buildBottomMenu(BuildContext context) {
    final navigator = Navigator.of(context);
    final localizations = BottlesLocalizations.of(context);

    return SafeArea(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          ListTile(
            title: Text(localizations.takeAPhoto),
            leading: Icon(FontAwesomeIcons.camera),
            onTap: () => navigator.pop(ImageSource.camera),
          ),
          ListTile(
            title: Text(localizations.selectFromGallery),
            leading: Icon(FontAwesomeIcons.solidImages),
            onTap: () => navigator.pop(ImageSource.gallery),
          ),
        ],
      ),
    );
  }

  Future<void> _selectPhoto(ImageSource source) async {
    final image = await ImagePicker.pickImage(source: source);
    if (image != null) {
      await _cropImage(image);
    }
  }

  Future<void> _cropImage(File image) async {
    final croppedImage = await ImageCropper.cropImage(
      sourcePath: image.path,
      ratioX: widget.ratioX,
      ratioY: widget.ratioY,
      maxWidth: widget.maxWidth,
      maxHeight: widget.maxHeight,
      circleShape: widget.circleShape,
    );
    widget.onImageSelected(croppedImage);
  }

  @override
  Widget build(BuildContext context) {
    Widget overlay;

    if (widget.useInkWell) {
      overlay = Material(
        type: MaterialType.transparency,
        child: InkWell(
          onTap: _showSelectionMenu,
        ),
      );
    } else {
      GestureDetector(
        onTap: _showSelectionMenu,
      );
    }

    return Stack(
      fit: StackFit.loose,
      children: [
        widget.child,
        Align(
          child: overlay,
          alignment: Alignment.center,
        ),
      ],
    );
  }
}
