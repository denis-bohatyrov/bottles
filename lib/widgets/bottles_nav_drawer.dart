import 'package:bottles/models/models.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

import 'package:bottles/blocs/blocs.dart';
import 'package:bottles/bottles_localizations.dart';
import 'package:bottles/screens/screens.dart';
import 'package:bottles/helpers/no_overscroll_behaviour.dart';
import 'package:bottles/widgets/waves.dart';

class BottlesNavDrawer extends StatefulWidget {
  @override
  _BottlesNavDrawerState createState() => _BottlesNavDrawerState();
}

class _BottlesNavDrawerState extends State<BottlesNavDrawer> {
  BottlesLocalizations get localizations => BottlesLocalizations.of(context);

  void _onSignOut() {
    final authBloc = Provider.of<AuthBloc>(context);
    authBloc.signOutSink.add(null);
    Navigator.of(context)
        .pushAndRemoveUntil(LoginScreen.buildPageRoute(), (_) => false);
  }

  void _onProfile() {
    Navigator.of(context)
      ..pop()
      ..push(ProfileScreen.buildPageRoute());
  }

  @override
  Widget build(BuildContext context) {
    final primaryColor = Theme.of(context).primaryColor;
    final usersBloc = Provider.of<UsersBloc>(context);

    return Drawer(
      child: Container(
        color: primaryColor,
        child: SafeArea(
          child: Container(
            color: Colors.white,
            child: Column(
              children: <Widget>[
                StreamBuilder<User>(
                  stream: usersBloc.currentUserStream,
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting ||
                        !snapshot.hasData) {
                      return Container();
                    }

                    final user = snapshot.data;

                    return GestureDetector(
                      onTap: _onProfile,
                      child: UserAccountsDrawerHeader(
                        accountEmail: Text(user.email),
                        accountName: Text(user.displayName ?? ''),
                        currentAccountPicture: CircleAvatar(
                          backgroundImage: user.photoUrl != null
                              ? NetworkImage(user.photoUrl)
                              : AssetImage('assets/bottle.png'),
                        ),
                        decoration: BoxDecoration(
                          color: primaryColor,
                        ),
                        margin: EdgeInsets.zero,
                      ),
                    );
                  },
                ),
                Waves(),
                Expanded(
                  child: Container(
                    color: Colors.white,
                    child: ScrollConfiguration(
                      behavior: NoOverScrollBehavior(),
                      child: ListView(
                        physics: ClampingScrollPhysics(),
                        children: <Widget>[
                          ListTile(
                            title: Text('ITEM A'),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                SafeArea(
                  child: ListTile(
                    title: Text(localizations.signOut),
                    leading: Icon(FontAwesomeIcons.signOutAlt),
                    onTap: _onSignOut,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
