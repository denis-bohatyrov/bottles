import 'package:flutter/material.dart';

class MaterialInputWrapper extends StatelessWidget {
  final Widget child;

  MaterialInputWrapper({
    Key key,
    @required this.child,
  })  : assert(child != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      borderRadius: BorderRadius.circular(10.0),
      type: MaterialType.card,
      elevation: 1,
      child: child,
    );
  }
}