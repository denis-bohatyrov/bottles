import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

// ignore: unnecessary_new
final messages = new MessageLookup();

// ignore: unused_element
final _keepAnalysisHappy = Intl.defaultLocale;

// ignore: non_constant_identifier_names
typedef String MessageIfAbsent(String message_str, List args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  static String passwordLengthError(int length) => Intl.plural(
    length,
    one: 'Password length must be greater that $length character',
    other: 'Password length must be greater that $length characters',
  );

  static String mustBeEqualTo(String value) => 'Must be equal to $value';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => {
    'appName': MessageLookupByLibrary.simpleMessage('Bottles'),
    'email': MessageLookupByLibrary.simpleMessage('Email'),
    'password': MessageLookupByLibrary.simpleMessage('Password'),
    'login': MessageLookupByLibrary.simpleMessage('Login'),
    'forgotPassword': MessageLookupByLibrary.simpleMessage('Forgot password?'),
    'dontHaveAccount': MessageLookupByLibrary.simpleMessage('''Don't have an account?'''),
    'signUpVerb': MessageLookupByLibrary.simpleMessage('Sign up'),
    'signUpNoun': MessageLookupByLibrary.simpleMessage('Sign up'),
    'confirmPassword': MessageLookupByLibrary.simpleMessage('Confirm password'),
    'restorePassword': MessageLookupByLibrary.simpleMessage('Restore password'),
    'restore': MessageLookupByLibrary.simpleMessage('Restore'),
    'required': MessageLookupByLibrary.simpleMessage('Required field'),
    'passwordLengthError': passwordLengthError,
    'mustBeEqualTo': mustBeEqualTo,
    'unknownError': MessageLookupByLibrary.simpleMessage('Unknown error'),
    'wrongCredentials': MessageLookupByLibrary.simpleMessage('Wrong email or password'),
    'tooManyRequests': MessageLookupByLibrary.simpleMessage('Too many requests, chill bro'),
    'emailInUse': MessageLookupByLibrary.simpleMessage('Email already in use'),
    'accountWithEmailNotFound': MessageLookupByLibrary.simpleMessage('Account with provided email not found'),
    'restoreEmailSent': MessageLookupByLibrary.simpleMessage('Restore link was sent to your email'),
    'signOut': MessageLookupByLibrary.simpleMessage('Sign out'),
    'profile': MessageLookupByLibrary.simpleMessage('Profile'),
    'save': MessageLookupByLibrary.simpleMessage('Save'),
    'fullName': MessageLookupByLibrary.simpleMessage('Full name'),
    'changePassword': MessageLookupByLibrary.simpleMessage('Change password'),
    'userInfo': MessageLookupByLibrary.simpleMessage('User info'),
    'reLoginException': MessageLookupByLibrary.simpleMessage('Please log in again to perform this operation'),
    'passwordChangeSuccess': MessageLookupByLibrary.simpleMessage('Password was successfully changed'),
    'takeAPhoto': MessageLookupByLibrary.simpleMessage('Take a photo'),
    'selectFromGallery': MessageLookupByLibrary.simpleMessage('Select from gallery'),
  };
}
