import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

// ignore: unnecessary_new
final messages = new MessageLookup();

// ignore: unused_element
final _keepAnalysisHappy = Intl.defaultLocale;

// ignore: non_constant_identifier_names
typedef String MessageIfAbsent(String message_str, List args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'ru';

  static String passwordLengthError(length) => Intl.plural(
    length,
    one: 'Пароль должен быть больше чем $length символ',
    few: 'Пароль должен быть больше чем $length символа',
    other: 'Пароль должен быть больше чем $length символов',
  );

  static String mustBeEqualTo(String value) => 'Должно совпадать с полем $value';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => {
    'appName': MessageLookupByLibrary.simpleMessage('Bottles'),
    'email': MessageLookupByLibrary.simpleMessage('Эл. почта'),
    'password': MessageLookupByLibrary.simpleMessage('Пароль'),
    'login': MessageLookupByLibrary.simpleMessage('Войти'),
    'forgotPassword': MessageLookupByLibrary.simpleMessage('Забыли пароль?'),
    'dontHaveAccount': MessageLookupByLibrary.simpleMessage('Ещё нет профиля?'),
    'signUpVerb': MessageLookupByLibrary.simpleMessage('Зарегистрироваться'),
    'signUpNoun': MessageLookupByLibrary.simpleMessage('Регистрация'),
    'confirmPassword': MessageLookupByLibrary.simpleMessage('Подтвердите пароль'),
    'restorePassword': MessageLookupByLibrary.simpleMessage('Восстановление пароля'),
    'restore': MessageLookupByLibrary.simpleMessage('Восстановить'),
    'required': MessageLookupByLibrary.simpleMessage('Обязательное поле'),
    'passwordLengthError': passwordLengthError,
    'mustBeEqualTo': mustBeEqualTo,
    'unknownError': MessageLookupByLibrary.simpleMessage('Неизвестная ошибка'),
    'wrongCredentials': MessageLookupByLibrary.simpleMessage('Неправильная почта или пароль'),
    'tooManyRequests': MessageLookupByLibrary.simpleMessage('Слишком много запросов, остынь дружище'),
    'emailInUse': MessageLookupByLibrary.simpleMessage('Адресс эл. почты уже занят'),
    'accountWithEmailNotFound': MessageLookupByLibrary.simpleMessage('Аккаунт с данной эл. почтой не найден'),
    'restoreEmailSent': MessageLookupByLibrary.simpleMessage('Ссылка для восстановления пароля отправлена вам на почту'),
    'signOut': MessageLookupByLibrary.simpleMessage('Выйти'),
    'profile': MessageLookupByLibrary.simpleMessage('Профиль'),
    'save': MessageLookupByLibrary.simpleMessage('Сохранить'),
    'fullName': MessageLookupByLibrary.simpleMessage('Полное имя'),
    'changePassword': MessageLookupByLibrary.simpleMessage('Изменить пароль'),
    'userInfo': MessageLookupByLibrary.simpleMessage('Данные пользователя'),
    'reLoginException': MessageLookupByLibrary.simpleMessage('Для данной операции вам необходимо перезайти в приложение'),
    'passwordChangeSuccess': MessageLookupByLibrary.simpleMessage('Пароль был успешно изменен'),
    'takeAPhoto': MessageLookupByLibrary.simpleMessage('Сделать фотографию'),
    'selectFromGallery': MessageLookupByLibrary.simpleMessage('Выбрать из галереи'),
  };
}
