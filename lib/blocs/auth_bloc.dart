import 'package:bottles/analytics.dart';
import 'package:meta/meta.dart';
import 'package:rxdart/rxdart.dart';

import 'package:bottles/api/auth_api.dart';
import 'package:bottles/models/models.dart';
import 'bloc_base.dart';

class AuthBloc implements BlocBase {
  final IAuthApi _authApi;
  final IAnalytics _analytics;

  final _loginSubject = PublishSubject<AuthPayload>();
  final _registerSubject = PublishSubject<EmailPasswordPayload>();
  final _resetPasswordSubject = PublishSubject<String>();
  final _signOutSubject = PublishSubject<void>();

  AuthBloc({
    @required IAuthApi authApi,
    @required IAnalytics analytics,
  })  : assert(authApi != null),
        assert(analytics != null),
        _authApi = authApi,
        _analytics = analytics {
    _loginSubject.listen(_handleLogin);
    _registerSubject.listen(_handleRegister);
    _resetPasswordSubject.listen(_handleResetPassword);
    _signOutSubject.listen(_handleSignOut);
  }

  Sink<AuthPayload> get loginWithEmailSink => _loginSubject.sink;

  Sink<EmailPasswordPayload> get registerSink => _registerSubject.sink;

  Sink<String> get resetPasswordSink => _resetPasswordSubject.sink;

  Sink<void> get signOutSink => _signOutSubject.sink;

  Stream<User> get loginResultStream => _loginSubject.switchMap(_handleLogin);

  Stream<void> get registerResultStream =>
      _registerSubject.switchMap(_handleRegister);

  Stream<void> get resetPasswordResultStream =>
      _resetPasswordSubject.switchMap(_handleResetPassword);

  Future<bool> isSignedIn() => _authApi.isSignedIn();

  Stream<User> _handleLogin(AuthPayload payload) async* {
    User user;
    if (payload is EmailPasswordPayload) {
      user = await _authApi.loginWithEmail(payload);
      await _analytics.sendLogin('email');
    }

    if (payload is GoogleLoginPayload) {
      user = await _authApi.loginOrRegisterWithGoogle();
      await _analytics.sendLogin('google');
    }

    yield user;
  }

  Stream<void> _handleRegister(EmailPasswordPayload payload) async* {
    await _authApi.registerWithEmail(payload);
    yield null;
  }

  Stream<void> _handleResetPassword(String email) async* {
    await _authApi.resetPassword(email);
    yield null;
  }

  Future<void> _handleSignOut(_) async {
    await _authApi.signOut();
  }

  @override
  void dispose() {
    _loginSubject.close();
    _registerSubject.close();
    _resetPasswordSubject.close();
    _signOutSubject.close();
  }
}
