import 'dart:async';
import 'dart:io';

import 'package:rxdart/rxdart.dart';

import 'package:bottles/models/models.dart';
import 'package:bottles/api/users_api.dart';
import 'package:bottles/blocs/bloc_base.dart';

class UsersBloc implements BlocBase {
  final IUsersApi _usersApi;
  final _updateUserInfoSubject = PublishSubject<UpdateUserInfo>();
  final _changePasswordInfoSubject = PublishSubject<String>();
  final _updatePhotoSubject = PublishSubject<File>();
  final _currentUserSubject = BehaviorSubject<User>();
  StreamSubscription _changeAuthStateSubscription;

  Sink<UpdateUserInfo> get updateUserInfoSink => _updateUserInfoSubject.sink;
  Sink<String> get changePasswordSink => _changePasswordInfoSubject.sink;
  Sink<File> get updatePhotoSink => _updatePhotoSubject.sink;

  Stream<User> get currentUserStream => _currentUserSubject.stream;
  Stream<void> get updateUserInfoResultStream => _updateUserInfoSubject.switchMap(_handleUpdateUserInfo);
  Stream<void> get changePasswordResultStream => _changePasswordInfoSubject.switchMap(_handleChangePassword);
  Stream<void> get updatePhotoResultStream => _updatePhotoSubject.switchMap(_handleUpdatePhoto);

  UsersBloc({
    IUsersApi usersApi,
  })  : assert(usersApi != null),
        _usersApi = usersApi {
    _changeAuthStateSubscription = _usersApi.currentUserStream().listen(_handleStateChange, onError: _handleStateChangeError,);
    _initCurrentUser();
  }

  Future<void> _initCurrentUser() async {
    final user = await _usersApi.currentUser();
    _currentUserSubject.add(user);
  }

  Stream<void> _handleUpdateUserInfo(UpdateUserInfo info) async* {
    final user = await _usersApi.currentUser();
    if (info.email != user.email) {
      await _usersApi.updateEmail(info.email);
    }

    if (info.fullName != user.displayName) {
      await _usersApi.updateName(info.fullName);
    }
    await _updateCurrentUserSubject();
    yield null;
  }

  Stream<void> _handleChangePassword(String password) async* {
    await _usersApi.changePassword(password);
    yield null;
  }

  Stream<void> _handleUpdatePhoto(File image) async* {
    await _usersApi.updateProfilePhoto(image);
    await _updateCurrentUserSubject();
    yield null;
  }

  void _handleStateChange(User user) {
    _currentUserSubject.add(user);
  }
  void _handleStateChangeError(Object error) {
    _currentUserSubject.addError(error);
  }

  Future<void> _updateCurrentUserSubject() async {
    final user = await _usersApi.currentUser();
    _currentUserSubject.add(user);
  }

  @override
  void dispose() {
    _updateUserInfoSubject.close();
    _changePasswordInfoSubject.close();
    _updatePhotoSubject.close();
    _currentUserSubject.close();
    _changeAuthStateSubscription.cancel();
  }
}
