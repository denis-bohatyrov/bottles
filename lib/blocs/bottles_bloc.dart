import 'package:meta/meta.dart';
import 'package:rxdart/rxdart.dart';

import 'package:bottles/api/bottles_api.dart';
import 'package:bottles/models/bottle/bottle.dart';
import 'package:bottles/blocs/bloc_base.dart';

class BottlesBloc implements BlocBase {
  final IBottlesApi _bottlesApi;

  BottlesBloc({
    @required IBottlesApi bottlesApi,
  })  : assert(bottlesApi != null),
        _bottlesApi = bottlesApi {
    _bottlesApi.bottles.listen(print);
  }

  Stream<List<Bottle>> get bottlesStream => _bottlesApi.bottles;

  @override
  void dispose() {
    // TODO: implement dispose
  }
}
