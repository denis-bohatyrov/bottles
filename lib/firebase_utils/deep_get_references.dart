import 'package:cloud_firestore/cloud_firestore.dart';

Future<Map<String, dynamic>> deepGetReferences(
    DocumentSnapshot document) async {
  Map<String, dynamic> data = {};
  for (String key in document.data.keys) {
    final value = document.data[key];
    if (value is DocumentReference) {
      final docSnapshot = await value.get();
      final docSnapshotData = await deepGetReferences(docSnapshot);
      data[key] = docSnapshotData;
    } else {
      data[key] = value;
    }
    data['uid'] = document.documentID;
  }

  return data;
}