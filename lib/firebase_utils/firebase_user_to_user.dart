import 'package:firebase_auth/firebase_auth.dart';

import 'package:bottles/models/models.dart';

User userInfoToUserAdapter(UserInfo userInfo) {
  return User(
    uid: userInfo.uid,
    email: userInfo.email,
    displayName: userInfo.displayName,
    photoUrl: userInfo.photoUrl,
  );
}