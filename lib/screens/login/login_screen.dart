import 'dart:async';
import 'dart:io';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter_sequence_animation/flutter_sequence_animation.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:bottles/blocs/auth_bloc.dart';
import 'package:bottles/errors.dart';
import 'package:bottles/bottles_localizations.dart';
import 'package:bottles/widgets/waves.dart';
import 'package:bottles/helpers/no_overscroll_behaviour.dart';
import 'package:bottles/screens/screens.dart';
import 'package:bottles/helpers/validation.dart';
import 'package:bottles/models/models.dart';
import 'package:bottles/variables.dart';
import 'package:provider/provider.dart';

enum LoginAnimationTags {
  offsetMultiplier,
  formOpacity,
}

class LoginScreen extends StatefulWidget {
  static PageRoute buildPageRoute() {
    if (Platform.isIOS) {
      return CupertinoPageRoute(builder: _builder);
    }

    return MaterialPageRoute(builder: _builder);
  }

  static Widget _builder(BuildContext context) {
    final authBloc = Provider.of<AuthBloc>(context);

    return LoginScreen(
      authBloc: authBloc,
    );
  }

  final AuthBloc authBloc;

  LoginScreen({
    Key key,
    @required this.authBloc,
  })  : assert(authBloc != null),
        super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  SequenceAnimation _sequenceAnimation;

  @override
  void initState() {
    _animationController = AnimationController(vsync: this);

    _sequenceAnimation = SequenceAnimationBuilder()
        .addAnimatable(
          animatable: Tween<double>(
            begin: 1,
            end: 0,
          ),
          from: Duration(),
          to: Duration(seconds: 1),
          tag: LoginAnimationTags.offsetMultiplier,
        )
        .addAnimatable(
          animatable: Tween<double>(
            begin: 0,
            end: 1,
          ),
          from: Duration(seconds: 1),
          to: Duration(seconds: 1, milliseconds: 500),
          tag: LoginAnimationTags.formOpacity,
        )
        .animate(_animationController);

    _animationController.forward();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final primaryColor = Theme.of(context).primaryColor;

    return Container(
      color: primaryColor,
      child: SafeArea(
        // Handle iPhone X bottom stuff
        bottom: false,
        child: LayoutBuilder(
          builder: (context, constraints) {
            return AnimatedBuilder(
              animation: _animationController,
              builder: (context, child) {
                final offsetMultiplier =
                    _sequenceAnimation[LoginAnimationTags.offsetMultiplier]
                        .value;
                final formOpacity =
                    _sequenceAnimation[LoginAnimationTags.formOpacity].value;

                return _AnimatableContainer(
                  offsetY: constraints.maxHeight * offsetMultiplier,
                  opacity: formOpacity,
                  child: child,
                );
              },
              child: _LoginForm(authBloc: widget.authBloc),
            );
          },
        ),
      ),
    );
  }
}

class _AnimatableContainer extends StatelessWidget {
  final double offsetY;
  final double opacity;
  final Widget child;

  _AnimatableContainer({
    Key key,
    @required this.offsetY,
    @required this.opacity,
    @required this.child,
  })  : assert(offsetY != null),
        assert(opacity != null),
        assert(child != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    final primaryColor = Theme.of(context).primaryColor;

    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: Transform.translate(
        offset: Offset(0, offsetY),
        child: LayoutBuilder(builder: (context, constraints) {
          return Scaffold(
            // white color is bug fix part of anti-aliasing issue https://github.com/flutter/flutter/issues/14288
            backgroundColor: Colors.white,
            body: ScrollConfiguration(
              behavior: NoOverScrollBehavior(),
              child: SingleChildScrollView(
                physics: ClampingScrollPhysics(),
                child: ConstrainedBox(
                  constraints: constraints,
                  child: RepaintBoundary(
                    child: Column(
                      children: [
                        Expanded(
                          flex: 3,
                          child: Container(
                            alignment: Alignment.center,
                            constraints: BoxConstraints(maxHeight: 200),
                            // not transparent because of bug fix part of anti-aliasing issue https://github.com/flutter/flutter/issues/14288
                            color: primaryColor,
                            child: Image.asset(
                              'assets/logo.png',
                              fit: BoxFit.fitHeight,
                            ),
                          ),
                        ),
                        Waves(),
                        Expanded(
                          flex: 5,
                          child: Container(
                            color: Colors.white,
                            child: Opacity(
                              opacity: opacity,
                              child: child,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          );
        }),
      ),
    );
  }
}

class _LoginForm extends StatefulWidget {
  final AuthBloc authBloc;

  _LoginForm({
    Key key,
    @required this.authBloc,
  })  : assert(authBloc != null),
        super(key: key);

  @override
  __LoginFormState createState() => __LoginFormState();
}

class __LoginFormState extends State<_LoginForm> with WidgetsBindingObserver {
  final _formStateKey = GlobalKey<FormState>();
  final _passwordFocusNode = FocusNode();
  final _signUpRecognizer = TapGestureRecognizer();

  bool _hidePassword = true;
  bool _hideErrors = true;
  bool _disableForm = false;
  EmailPasswordPayload _emailPasswordPayload = EmailPasswordPayload();
  StreamSubscription _errorsStreamSubscription;

  @override
  void initState() {
    _signUpRecognizer.onTap = _onSignUp;
    _errorsStreamSubscription = widget.authBloc.loginResultStream
        .listen(_onLoginSuccess, onError: _onLoginError);

    super.initState();
  }

  BottlesLocalizations get localizations => BottlesLocalizations.of(context);

  void _onEmailLogin() {
    FocusScope.of(context).requestFocus(FocusNode());

    setState(() {
      _hideErrors = false;
    });
    if (_formStateKey.currentState.validate()) {
      _formStateKey.currentState.save();
      setState(() {
        _disableForm = true;
      });
      widget.authBloc.loginWithEmailSink.add(_emailPasswordPayload);
    }
  }

  void _onGoogleLogin() {
    setState(() {
      _disableForm = true;
    });
    widget.authBloc.loginWithEmailSink.add(GoogleLoginPayload());
  }

  void _onSignUp() {
    Navigator.of(context).push(SignUpScreen.buildPageRoute());
  }

  void _onForgotPassword() {
    Navigator.of(context).push(RestorePasswordScreen.buildPageRoute());
  }

  void _onTogglePassword() {
    setState(() {
      _hidePassword = !_hidePassword;
    });
  }

  void _onEmailEndEditing() {
    FocusScope.of(context).requestFocus(_passwordFocusNode);
  }

  void _onLoginSuccess(_) {
    setState(() {
      _disableForm = false;
    });

    Navigator.of(context).pushAndRemoveUntil(HomeScreen.buildPageRoute(), (_) => false);
  }

  void _onLoginError(Object error) {
    setState(() {
      _disableForm = false;
    });
    String errorMessage;

    if (error is WrongCredentialsException) {
      errorMessage = localizations.wrongCredentials;
    } else if (error is TooManyRequestsException) {
      errorMessage = localizations.tooManyRequests;
    } else if (error is GoogleSignInCanceledException) {
    } else {
      errorMessage = localizations.unknownError;
    }

    if (errorMessage != null) {
      final snackBar = SnackBar(content: Text(errorMessage));
      Scaffold.of(context).showSnackBar(snackBar);
    }
  }

  void _resetErrors() {
    if (!_hideErrors) {
      setState(() {
        _hideErrors = true;
      });
      _formStateKey.currentState.validate();
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        constraints: BoxConstraints(maxWidth: MAX_FORM_WIDTH),
        padding: EdgeInsets.symmetric(horizontal: 40, vertical: 10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Form(
              key: _formStateKey,
              onChanged: _resetErrors,
              child: Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    _buildEmailField(),
                    Divider(),
                    _buildPasswordField(),
                    Divider(height: 32.0),
                    _buildLoginButtons(),
                    Divider(height: 32.0),
                    _buildForgotPassword(),
                  ],
                ),
              ),
            ),
            _buildSignUpInfo(),
          ],
        ),
      ),
    );
  }

  TextFormField _buildEmailField() {
    return TextFormField(
      enabled: !_disableForm,
      validator: _emailValidator,
      onEditingComplete: _onEmailEndEditing,
      keyboardType: TextInputType.emailAddress,
      textInputAction: TextInputAction.next,
      onSaved: (email) => _emailPasswordPayload.email = email,
      decoration: InputDecoration(
        prefixIcon: Icon(Icons.email),
        hintText: localizations.email,
      ),
    );
  }

  String _emailValidator(String value) {
    return _hideErrors
        ? null
        : validate<String>(context, value, [
            requiredField,
            isValidEmail,
          ]);
  }

  Widget _buildPasswordField() {
    final icon = _hidePassword ? Icons.lock : Icons.lock_open;

    return TextFormField(
      enabled: !_disableForm,
      obscureText: _hidePassword,
      validator: _passwordValidator,
      focusNode: _passwordFocusNode,
      onSaved: (password) => _emailPasswordPayload.password = password,
      onEditingComplete: _onEmailLogin,
      decoration: InputDecoration(
        prefixIcon: IconButton(
          onPressed: _onTogglePassword,
          icon: Icon(icon),
        ),
        hintText: localizations.password,
      ),
    );
  }

  String _passwordValidator(String value) {
    return _hideErrors
        ? null
        : validate<String>(context, value, [
            requiredField,
          ]);
  }

  Widget _buildForgotPassword() {
    final accentColor = Theme.of(context).accentColor;

    return GestureDetector(
      onTap: _onForgotPassword,
      child: Text(
        localizations.forgotPassword,
        style: TextStyle(color: accentColor),
      ),
    );
  }

  Widget _buildSignUpInfo() {
    final accentColor = Theme.of(context).accentColor;

    return RichText(
      text: TextSpan(
        text: '${localizations.dontHaveAccount} ',
        style: TextStyle(color: Colors.black),
        children: [
          TextSpan(
            style: TextStyle(color: accentColor),
            text: localizations.signUpVerb,
            recognizer: _signUpRecognizer,
          ),
        ],
      ),
    );
  }

  Widget _buildLoginButtons() {
    final accentColor = Theme.of(context).accentColor;
    final disabledColor = Theme.of(context).disabledColor;

    return Row(
      children: <Widget>[
        Expanded(
          child: FlatButton(
            padding: EdgeInsets.symmetric(vertical: 20),
            color: accentColor,
            disabledColor: disabledColor,
            onPressed: _disableForm ? null : _onEmailLogin,
            child: Text(
              localizations.login,
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
        SizedBox(width: 16.0),
        ClipRRect(
          borderRadius: BorderRadius.circular(50),
          child: ButtonTheme(
            minWidth: 0.0,
            child: FlatButton(
              padding: EdgeInsets.all(20),
              color: accentColor,
              disabledColor: disabledColor,
              onPressed: _disableForm ? null : _onGoogleLogin,
              child: Icon(
                FontAwesomeIcons.google,
                color: Colors.white,
              ),
//          shape: CircleBorder(),
            ),
          ),
        ),
      ],
    );
  }

  @override
  void dispose() {
    _errorsStreamSubscription.cancel();
    super.dispose();
  }
}
