import 'dart:async';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:bottles/blocs/auth_bloc.dart';
import 'package:bottles/errors.dart';
import 'package:bottles/helpers/validation.dart';
import 'package:bottles/bottles_localizations.dart';
import 'package:bottles/helpers/no_overscroll_behaviour.dart';

class RestorePasswordScreen extends StatefulWidget {
  static PageRoute buildPageRoute() {
    if (Platform.isIOS) {
      return CupertinoPageRoute(builder: _builder);
    }

    return MaterialPageRoute(builder: _builder);
  }

  static Widget _builder(BuildContext context) {
    final authBloc = Provider.of<AuthBloc>(context);

    return RestorePasswordScreen(
      authBloc: authBloc,
    );
  }

  final AuthBloc authBloc;

  RestorePasswordScreen({
    Key key,
    @required this.authBloc,
  })  : assert(authBloc != null),
        super(key: key);

  @override
  _RestorePasswordScreenState createState() => _RestorePasswordScreenState();
}

class _RestorePasswordScreenState extends State<RestorePasswordScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _RestorePasswordForm(
        authBloc: widget.authBloc,
      ),
    );
  }
}

class _RestorePasswordForm extends StatefulWidget {
  final AuthBloc authBloc;

  _RestorePasswordForm({
    Key key,
    @required this.authBloc,
  })  : assert(authBloc != null),
        super(key: key);

  @override
  __RestorePasswordFormState createState() => __RestorePasswordFormState();
}

class __RestorePasswordFormState extends State<_RestorePasswordForm> {
  final _formStateKey = GlobalKey<FormState>();
  String _email;
  bool _disableForm = false;
  bool _hideErrors = true;
  StreamSubscription _restorePasswordResultSubscription;

  BottlesLocalizations get localizations => BottlesLocalizations.of(context);

  @override
  void initState() {
    _restorePasswordResultSubscription = widget
        .authBloc.resetPasswordResultStream
        .listen(_onRestoreSuccess, onError: _onRestoreError);
    super.initState();
  }

  void _onRestorePassword() {
    if (_formStateKey.currentState.validate()) {
      _formStateKey.currentState.save();

      setState(() {
        _disableForm = true;
      });

      widget.authBloc.resetPasswordSink.add(_email);
    }
  }

  void _onRestoreSuccess(_) {
    setState(() {
      _disableForm = false;
    });
    final snackBar = SnackBar(content: Text(localizations.restoreEmailSent));
    Scaffold.of(context).showSnackBar(snackBar);
  }

  void _onRestoreError(Object error) {
      setState(() {
        _disableForm = false;
      });
      String errorMessage;

      if (error is WrongCredentialsException) {
        errorMessage = localizations.accountWithEmailNotFound;
      } else {
        errorMessage = localizations.unknownError;
      }

      if (errorMessage != null) {
        final snackBar = SnackBar(content: Text(errorMessage));
        Scaffold.of(context).showSnackBar(snackBar);
      }
  }

  void _resetErrors() {
    if (!_hideErrors) {
      setState(() {
        _hideErrors = true;
      });
      _formStateKey.currentState.validate();
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
      child: Container(
        color: Theme.of(context).scaffoldBackgroundColor,
        child: SafeArea(
          child: LayoutBuilder(builder: (context, constraints) {
            return Scaffold(
              body: ScrollConfiguration(
                behavior: NoOverScrollBehavior(),
                child: SingleChildScrollView(
                  physics: ClampingScrollPhysics(),
                  child: Form(
                    key: _formStateKey,
                    onChanged: _resetErrors,
                    child: Center(
                      child: Container(
                        constraints:
                            constraints.copyWith(minWidth: 0, maxWidth: 500),
                        padding:
                            EdgeInsets.symmetric(horizontal: 40, vertical: 10),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            _buildHeader(),
                            Divider(
                              height: 32.0,
                            ),
                            _buildEmailField(),
                            Divider(
                              height: 32.0,
                            ),
                            _buildRestoreButton(),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            );
          }),
        ),
      ),
    );
  }

  TextFormField _buildEmailField() {
    return TextFormField(
      enabled: !_disableForm,
      validator: _emailValidator,
      onEditingComplete: _onRestorePassword,
      keyboardType: TextInputType.emailAddress,
      onSaved: (email) => _email = email,
      decoration: InputDecoration(
        hintText: localizations.email,
        prefixIcon: Icon(Icons.email),
      ),
    );
  }

  String _emailValidator(String email) {
    return _hideErrors
        ? null
        : validate<String>(context, email, [
            requiredField,
            isValidEmail,
          ]);
  }

  Widget _buildHeader() {
    final headerStyle = Theme.of(context)
        .primaryTextTheme
        .display1
        .copyWith(color: Colors.black87);

    return Text(
      localizations.restorePassword,
      style: headerStyle,
      textAlign: TextAlign.center,
    );
  }

  Widget _buildRestoreButton() {
    final accentColor = Theme.of(context).accentColor;
    final disabledColor = Theme.of(context).disabledColor;

    return Container(
      width: double.infinity,
      padding: EdgeInsets.symmetric(vertical: 0),
      child: FlatButton(
        padding: EdgeInsets.symmetric(vertical: 20),
        color: accentColor,
        disabledColor: disabledColor,
        onPressed: _disableForm ? null : _onRestorePassword,
        child: Text(
          localizations.restore,
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _restorePasswordResultSubscription.cancel();
    super.dispose();
  }
}
