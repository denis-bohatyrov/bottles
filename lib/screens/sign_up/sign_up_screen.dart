import 'dart:async';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:bottles/errors.dart';
import 'package:bottles/models/auth_payload.dart';
import 'package:bottles/blocs/blocs.dart';
import 'package:bottles/bottles_localizations.dart';
import 'package:bottles/helpers/validation.dart';
import 'package:bottles/variables.dart';
import 'package:bottles/helpers/no_overscroll_behaviour.dart';
import 'package:bottles/screens/screens.dart' show HomeScreen;

class SignUpScreen extends StatefulWidget {
  static PageRoute buildPageRoute() {
    if (Platform.isIOS) {
      return CupertinoPageRoute(builder: _builder);
    }

    return MaterialPageRoute(builder: _builder);
  }

  static Widget _builder(BuildContext context) {
    final authBloc = Provider.of<AuthBloc>(context);

    return SignUpScreen(
      authBloc: authBloc,
    );
  }

  final AuthBloc authBloc;

  SignUpScreen({
    Key key,
    @required this.authBloc,
  })  : assert(authBloc != null),
        super(key: key);

  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _RegisterForm(
        authBloc: widget.authBloc,
      ),
    );
  }
}

class _RegisterForm extends StatefulWidget {
  final AuthBloc authBloc;

  _RegisterForm({
    Key key,
    @required this.authBloc,
  })  : assert(authBloc != null),
        super(key: key);

  @override
  __RegisterFormState createState() => __RegisterFormState();
}

class __RegisterFormState extends State<_RegisterForm> {
  final _formStateKey = GlobalKey<FormState>();
  final _passwordStateKey = GlobalKey<FormFieldState>();
  final _passwordFocusNode = FocusNode();
  final _rePasswordFocusNode = FocusNode();

  bool _hidePassword = true;
  bool _hideRePassword = true;
  bool _hideErrors = true;
  bool _disableForm = false;
  EmailPasswordPayload _emailPasswordPayload = EmailPasswordPayload();
  StreamSubscription _registerResultSubscription;

  BottlesLocalizations get localizations => BottlesLocalizations.of(context);

  @override
  void initState() {
    _registerResultSubscription = widget.authBloc.registerResultStream.listen(
      _onRegisterSuccess,
      onError: _onRegisterError,
    );

    super.initState();
  }

  void _onTogglePassword() {
    setState(() {
      _hidePassword = !_hidePassword;
    });
  }

  void _onToggleRePassword() {
    setState(() {
      _hideRePassword = !_hideRePassword;
    });
  }

  void _onSignUp() {
    _hideKeyboard();
    setState(() {
      _hideErrors = false;
    });
    if (_formStateKey.currentState.validate()) {
      _formStateKey.currentState.save();

      setState(() {
        _disableForm = true;
      });

      widget.authBloc.registerSink.add(_emailPasswordPayload);
    }
  }

  void _hideKeyboard() {
    FocusScope.of(context).requestFocus(FocusNode());
  }

  void _onEmailEndEditing() =>
      FocusScope.of(context).requestFocus(_passwordFocusNode);

  void _onPasswordEndEditing() =>
      FocusScope.of(context).requestFocus(_rePasswordFocusNode);

  void _onRegisterSuccess(_) {
    setState(() {
      _disableForm = false;
    });

    Navigator.of(context).pushAndRemoveUntil(HomeScreen.buildPageRoute(), (_) => false);
  }

  void _onRegisterError(Object error) {
    setState(() {
      _disableForm = false;
    });
    String errorMessage;

    if (error is EmailAlreadyInUseException) {
      errorMessage = localizations.emailInUse;
    } else {
      errorMessage = localizations.unknownError;
    }

    if (errorMessage != null) {
      final snackBar = SnackBar(content: Text(errorMessage));
      Scaffold.of(context).showSnackBar(snackBar);
    }
  }

  void _resetErrors() {
    if (!_hideErrors) {
      setState(() {
        _hideErrors = true;
      });
      _formStateKey.currentState.validate();
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: _hideKeyboard,
      child: Container(
        color: Theme.of(context).scaffoldBackgroundColor,
        child: SafeArea(
          child: LayoutBuilder(builder: (context, constraints) {
            return Scaffold(
              body: ScrollConfiguration(
                behavior: NoOverScrollBehavior(),
                child: SingleChildScrollView(
                  physics: ClampingScrollPhysics(),
                  child: Center(
                    child: Form(
                      key: _formStateKey,
                      onChanged: _resetErrors,
                      child: Container(
                        constraints: constraints.copyWith(
                            minWidth: 0.0, maxWidth: MAX_FORM_WIDTH),
                        padding:
                            EdgeInsets.symmetric(horizontal: 40, vertical: 10),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            _buildHeader(),
                            Divider(height: 32.0),
                            _buildEmailField(),
                            Divider(),
                            _buildPasswordField(),
                            Divider(),
                            _buildRePasswordField(),
                            Divider(height: 32.0),
                            _buildSignUpButton(),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            );
          }),
        ),
      ),
    );
  }

  TextFormField _buildEmailField() {
    return TextFormField(
      enabled: !_disableForm,
      validator: _emailValidator,
      onEditingComplete: _onEmailEndEditing,
      keyboardType: TextInputType.emailAddress,
      textInputAction: TextInputAction.next,
      onSaved: (email) => _emailPasswordPayload.email = email,
      decoration: InputDecoration(
        hintText: localizations.email,
        prefixIcon: Icon(Icons.email),
      ),
    );
  }

  String _emailValidator(String email) {
    return _hideErrors
        ? null
        : validate<String>(context, email, [
            requiredField,
            isValidEmail,
          ]);
  }

  TextFormField _buildPasswordField() {
    return TextFormField(
      key: _passwordStateKey,
      enabled: !_disableForm,
      focusNode: _passwordFocusNode,
      obscureText: _hidePassword,
      validator: _passwordValidator,
      onEditingComplete: _onPasswordEndEditing,
      textInputAction: TextInputAction.next,
      onSaved: (password) => _emailPasswordPayload.password = password,
      decoration: InputDecoration(
        hintText: localizations.password,
        prefixIcon: IconButton(
          onPressed: _onTogglePassword,
          icon: Icon(_hidePassword ? Icons.lock : Icons.lock_open),
        ),
      ),
    );
  }

  String _passwordValidator(String password) {
    return _hideErrors
        ? null
        : validate<String>(context, password, [
            requiredField,
            minLength(MIN_PASSWORD_LENGTH),
          ]);
  }

  TextFormField _buildRePasswordField() {
    return TextFormField(
      focusNode: _rePasswordFocusNode,
      enabled: !_disableForm,
      obscureText: _hideRePassword,
      validator: _rePasswordValidator,
      onEditingComplete: _onSignUp,
      decoration: InputDecoration(
        hintText: localizations.confirmPassword,
        prefixIcon: IconButton(
          onPressed: _onToggleRePassword,
          icon: Icon(_hideRePassword ? Icons.lock : Icons.lock_open),
        ),
      ),
    );
  }

  String _rePasswordValidator(String password) {
    return _hideErrors
        ? null
        : validate<String>(context, password, [
            mustBeEqualTo(
                _passwordStateKey.currentState.value, localizations.password),
          ]);
  }

  Widget _buildHeader() {
    final headerStyle = Theme.of(context)
        .primaryTextTheme
        .display1
        .copyWith(color: Colors.black87);

    return Text(
      localizations.signUpNoun,
      style: headerStyle,
    );
  }

  Widget _buildSignUpButton() {
    final accentColor = Theme.of(context).accentColor;
    final disabledColor = Theme.of(context).disabledColor;

    return Container(
      width: double.infinity,
      padding: EdgeInsets.symmetric(vertical: 0),
      child: FlatButton(
        padding: EdgeInsets.symmetric(vertical: 20),
        color: accentColor,
        disabledColor: disabledColor,
        onPressed: _disableForm ? null : _onSignUp,
        child: Text(
          localizations.signUpVerb,
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _registerResultSubscription.cancel();

    super.dispose();
  }
}
