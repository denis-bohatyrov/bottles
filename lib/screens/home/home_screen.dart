import 'dart:io';

import 'package:bottles/blocs/blocs.dart';
import 'package:bottles/models/models.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:bottles/widgets/bottles_nav_drawer.dart';
import 'package:bottles/screens/home/bottle_widget.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  static PageRoute buildPageRoute() {
    if (Platform.isIOS) {
      return CupertinoPageRoute(builder: _builder);
    }

    return MaterialPageRoute(builder: _builder);
  }

  static Widget _builder(BuildContext context) {
    final bottlesBloc = Provider.of<BottlesBloc>(context);
    return HomeScreen(
      bottlesBloc: bottlesBloc,
    );
  }

  final BottlesBloc bottlesBloc;

  HomeScreen({Key key, @required this.bottlesBloc})
      : assert(bottlesBloc != null),
        super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState(bottlesBloc.bottlesStream);
}

class _HomeScreenState extends State<HomeScreen> {
  final Stream<List<Bottle>> _bottlesScream;

  _HomeScreenState(this._bottlesScream);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Image.asset(
          'assets/bottles_text.png',
          height: 25.0,
        ),
        centerTitle: true,
      ),
      drawer: BottlesNavDrawer(),
      body: StreamBuilder<List<Bottle>>(
        stream: _bottlesScream,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting ||
              !snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }

          final bottles = snapshot.data;

          return GridView.builder(
            padding: EdgeInsets.all(10.0),
            itemCount: bottles.length,
            gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
              maxCrossAxisExtent: 200,
              childAspectRatio: 0.7,
              crossAxisSpacing: 10,
              mainAxisSpacing: 10,
            ),
            itemBuilder: (context, index) {
              final bottle = bottles[index];

              return BottleWidget(
                bottle: bottle,
              );
            },
          );
        },
      ),
    );
  }
}
