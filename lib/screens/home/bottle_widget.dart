import 'package:flutter/material.dart';

import 'package:bottles/widgets/bottle_ink_splash_factory.dart';
import 'package:bottles/models/models.dart';

class BottleWidget extends StatelessWidget {
  final Bottle bottle;
  final borderRadius = BorderRadius.circular(10.0);

  BottleWidget({
    Key key,
    @required this.bottle,
  })  : assert(bottle != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return Container(
      padding: EdgeInsets.all(8.0),
      child: Stack(
        children: <Widget>[
          Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Expanded(child: _buildImage()),
              Divider(
                height: 10,
              ),
              _buildText(theme),
            ],
          ),
          Material(
            type: MaterialType.transparency,
            clipBehavior: Clip.antiAlias,
            child: InkWell(
              onTap: () {},
              borderRadius: borderRadius,
              splashColor: theme.accentColor.withOpacity(0.4),
              highlightColor: theme.accentColor.withOpacity(0.05),
              splashFactory: BottleInkSplashFactory(),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildImage() {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: borderRadius,
        boxShadow: [
          BoxShadow(
            offset: Offset(0, 5),
            blurRadius: 10.0,
            color: Colors.black.withOpacity(0.10),
          ),
        ],
      ),
      child: ClipRRect(
        borderRadius: borderRadius,
        child: Image.network(
          bottle.images.first,
          fit: BoxFit.fill,
        ),
      ),
    );
  }

  Widget _buildText(ThemeData theme) {
    final titleStyle = theme.textTheme.title;
    final captionStyle = theme.textTheme.caption;

    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text(bottle.name ?? '', style: titleStyle),
          Text('${bottle.price.toStringAsFixed(2)}\$' ?? '',
              style: captionStyle),
        ],
      ),
    );
  }
}
