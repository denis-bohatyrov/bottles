import 'dart:async';

import 'package:bottles/variables.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:bottles/blocs/blocs.dart';
import 'package:bottles/bottles_localizations.dart';
import 'package:bottles/errors.dart';
import 'package:bottles/helpers/validation.dart';
import 'package:bottles/widgets/material_input_wrapper.dart';

class ChangePasswordForm extends StatefulWidget {
  final UsersBloc usersBloc;

  ChangePasswordForm({
    Key key,
    @required this.usersBloc,
  })  : assert(usersBloc != null),
        super(key: key);

  @override
  _ChangePasswordFormState createState() => _ChangePasswordFormState();
}

class _ChangePasswordFormState extends State<ChangePasswordForm> {
  final _formStateKey = GlobalKey<FormState>();
  final _passwordFieldKey = GlobalKey<FormFieldState>();
  final _rePasswordFocusNode = FocusNode();

  bool _hidePassword = true;
  bool _hideRePassword = true;
  bool _hideErrors = true;
  bool _disableForm = false;
  String _password;
  StreamSubscription _changePasswordResultSubscription;

  BottlesLocalizations get localizations => BottlesLocalizations.of(context);

  @override
  void initState() {
    _changePasswordResultSubscription =
        widget.usersBloc.changePasswordResultStream.listen(
          _handleSuccess,
          onError: _handleError,
        );
    super.initState();
  }

  void _onTogglePassword() {
    setState(() {
      _hidePassword = !_hidePassword;
    });
  }

  void _onToggleRePassword() {
    setState(() {
      _hideRePassword = !_hideRePassword;
    });
  }

  void _onPasswordEndEditing() =>
      FocusScope.of(context).requestFocus(_rePasswordFocusNode);

  void _onChangePassword() {
    setState(() {
      _hideErrors = false;
    });
    if (_formStateKey.currentState.validate()) {
      _formStateKey.currentState.save();

      setState(() {
        _disableForm = true;
      });
      widget.usersBloc.changePasswordSink.add(_password);
    }
  }

  void _handleSuccess(_) {
    setState(() {
      _disableForm = false;
    });
    _formStateKey.currentState.reset();

    final snackbar = SnackBar(content: Text(localizations.passwordChangeSuccess));
    Scaffold.of(context).showSnackBar(snackbar);
  }

  void _handleError(Object error) {
    setState(() {
      _disableForm = false;
    });

    String errorText;
    if (error is RequiresRecentLoginException) {
      errorText = localizations.reLoginException;
    } else {
      errorText = localizations.unknownError;
    }

    final snackbar = SnackBar(content: Text(errorText));
    Scaffold.of(context).showSnackBar(snackbar);
  }

  void _resetErrors() {
    if (!_hideErrors) {
      setState(() {
        _hideErrors = true;
      });
      _formStateKey.currentState.validate();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formStateKey,
      onChanged: _resetErrors,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Container(
            width: double.infinity,
            child: Text(
              localizations.changePassword,
              style: Theme.of(context).textTheme.subhead,
              textAlign: TextAlign.left,
            ),
          ),
          Divider(),
          _buildPasswordField(),
          Divider(),
          _buildConfirmPassword(),
          Divider(),
          Container(
            child: FlatButton(
              onPressed: _disableForm ? null : _onChangePassword,
              color: Theme.of(context).primaryColor,
              disabledColor: Theme.of(context).disabledColor,
              child: Text(
                localizations.save,
                style: TextStyle(color: Colors.white),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildPasswordField() {
    return MaterialInputWrapper(
      child: TextFormField(
        key: _passwordFieldKey,
        enabled: !_disableForm,
        obscureText: _hidePassword,
        validator: _passwordValidator,
        onEditingComplete: _onPasswordEndEditing,
        textInputAction: TextInputAction.next,
        onSaved: (password) => _password = password,
        decoration: InputDecoration(
          labelText: localizations.password,
          prefixIcon: IconButton(
            onPressed: _onTogglePassword,
            icon: Icon(_hidePassword ? Icons.lock : Icons.lock_open),
          ),
        ),
      ),
    );
  }

  String _passwordValidator(String password) {
    return _hideErrors
        ? null
        : validate<String>(context, password, [
      requiredField,
      minLength(MIN_PASSWORD_LENGTH),
    ]);
  }

  Widget _buildConfirmPassword() {
    return MaterialInputWrapper(
      child: TextFormField(
        focusNode: _rePasswordFocusNode,
        enabled: !_disableForm,
        obscureText: _hideRePassword,
        validator: _rePasswordValidator,
        decoration: InputDecoration(
          labelText: localizations.confirmPassword,
          prefixIcon: IconButton(
            onPressed: _onToggleRePassword,
            icon: Icon(_hideRePassword ? Icons.lock : Icons.lock_open),
          ),
        ),
      ),
    );
  }

  String _rePasswordValidator(String password) {
    return _hideErrors
        ? null
        : validate<String>(context, password, [
      mustBeEqualTo(
        _passwordFieldKey.currentState.value,
        localizations.password,
      ),
    ]);
  }

  @override
  void dispose() {
    _changePasswordResultSubscription.cancel();
    super.dispose();
  }
}