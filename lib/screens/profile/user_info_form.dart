import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:bottles/blocs/blocs.dart';
import 'package:bottles/bottles_localizations.dart';
import 'package:bottles/errors.dart';
import 'package:bottles/helpers/validation.dart';
import 'package:bottles/models/models.dart';
import 'package:bottles/widgets/material_input_wrapper.dart';

class UserInfoForm extends StatefulWidget {
  final UsersBloc usersBloc;

  UserInfoForm({
    Key key,
    @required this.usersBloc,
  })  : assert(usersBloc != null),
        super(key: key);

  @override
  _UserInfoFormState createState() => _UserInfoFormState();
}

class _UserInfoFormState extends State<UserInfoForm> {
  final _formStateKey = GlobalKey<FormState>();
  UpdateUserInfo _userInfo = UpdateUserInfo();
  StreamSubscription _userInfoUpdateResultSubscription;

  bool _disabledForm = false;
  bool _hideErrors = true;

  BottlesLocalizations get localizations => BottlesLocalizations.of(context);

  @override
  void initState() {
    _userInfoUpdateResultSubscription =
        widget.usersBloc.updateUserInfoResultStream.listen(
      _handleSuccess,
      onError: _handleError,
    );
    super.initState();
  }

  void _onSave() {
    setState(() {
      _hideErrors = false;
    });
    if (_formStateKey.currentState.validate()) {
      _formStateKey.currentState.save();
      widget.usersBloc.updateUserInfoSink.add(_userInfo);
    }
  }

  void _handleSuccess(_) {
    setState(() {
      _disabledForm = false;
    });
  }

  void _handleError(Object error) {
    setState(() {
      _disabledForm = false;
    });

    String errorText;
    if (error is EmailAlreadyInUseException) {
      errorText = localizations.emailInUse;
    } else if (error is RequiresRecentLoginException) {
      errorText = localizations.reLoginException;
    } else {
      errorText = localizations.unknownError;
    }

    _formStateKey.currentState.reset();

    final snackbar = SnackBar(content: Text(errorText));
    Scaffold.of(context).showSnackBar(snackbar);
  }

  void _resetErrors() {
    if (!_hideErrors) {
      setState(() {
        _hideErrors = true;
      });
      _formStateKey.currentState.validate();
    }
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<User>(
      stream: widget.usersBloc.currentUserStream,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting || !snapshot.hasData) {
          return SizedBox();
        }

        final user = snapshot.data;

        return Form(
          key: _formStateKey,
          onChanged: _resetErrors,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Container(
                width: double.infinity,
                child: Text(
                  localizations.userInfo,
                  style: Theme.of(context).textTheme.subhead,
                  textAlign: TextAlign.left,
                ),
              ),
              Divider(),
              _buildNameInput(user.displayName),
              Divider(),
              _buildEmailField(user.email),
              Divider(),
              Container(
                child: FlatButton(
                  onPressed: _disabledForm ? null : _onSave,
                  color: Theme.of(context).primaryColor,
                  child: Text(
                    localizations.save,
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
            ],
          ),
        );
      }
    );
  }

  MaterialInputWrapper _buildNameInput(String fullName) {
    return MaterialInputWrapper(
      child: TextFormField(
        enabled: !_disabledForm,
        initialValue: fullName,
        validator: _nameValidator,
        onSaved: (name) => _userInfo.fullName = name,
        decoration: InputDecoration(
          labelText: localizations.fullName,
        ),
      ),
    );
  }

  String _nameValidator(String name) {
    return _hideErrors
        ? null
        : validate<String>(context, name, [
            requiredField,
          ]);
  }

  MaterialInputWrapper _buildEmailField(String email) {
    return MaterialInputWrapper(
      child: TextFormField(
        enabled: !_disabledForm,
        initialValue: email,
        validator: _emailValidator,
        onSaved: (email) => _userInfo.email = email,
        decoration: InputDecoration(labelText: localizations.email),
      ),
    );
  }

  String _emailValidator(String email) {
    return _hideErrors
        ? null
        : validate<String>(context, email, [
            requiredField,
            isValidEmail,
          ]);
  }

  @override
  void dispose() {
    _userInfoUpdateResultSubscription.cancel();
    super.dispose();
  }
}
