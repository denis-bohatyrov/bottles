import 'dart:async';
import 'dart:io';

import 'package:bottles/models/models.dart';
import 'package:bottles/widgets/image_picker_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:bottles/blocs/blocs.dart';
import 'package:bottles/bottles_localizations.dart';

final double _kAvatarRadius = 60.0;

class AvatarForm extends StatefulWidget {
  final UsersBloc usersBloc;

  AvatarForm({
    Key key,
    @required this.usersBloc,
  })  : assert(usersBloc != null),
        super(key: key);

  @override
  _AvatarFormState createState() => _AvatarFormState();
}

class _AvatarFormState extends State<AvatarForm> {
  StreamSubscription _updatePhotoResultSubscription;
  bool _disablePhoto = false;

  @override
  void initState() {
    _updatePhotoResultSubscription = widget.usersBloc.updatePhotoResultStream
        .listen(_handlePhotoUpdateResult);
    super.initState();
  }

  BottlesLocalizations get localizations => BottlesLocalizations.of(context);

  void _onImageSelected(File image) {
    setState(() {
      _disablePhoto = true;
    });
    widget.usersBloc.updatePhotoSink.add(image);
  }

  void _handlePhotoUpdateResult(_) {
    setState(() {
      _disablePhoto = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<User>(
      stream: widget.usersBloc.currentUserStream,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting ||
            !snapshot.hasData) {
          return SizedBox();
        }

        final user = snapshot.data;
        Widget child;

        if (_disablePhoto) {
          child = SizedBox();
        } else if (user.photoUrl != null) {
          child = SizedBox.expand(
            child: Image.network(
              '${user.photoUrl}',
              fit: BoxFit.fill,
            ),
          );
        } else {
          child = Icon(
            Icons.add,
            color: Colors.grey[500],
          );
        }

        return ConstrainedBox(
          constraints: BoxConstraints(maxHeight: _kAvatarRadius * 2),
          child: LayoutBuilder(builder: (context, constraints) {
            return Stack(
              children: <Widget>[
                Container(color: Colors.red, height: constraints.maxHeight / 2),
                Align(
                  alignment: Alignment.center,
                  child: SizedBox(
                    width: constraints.maxHeight,
                    height: constraints.maxHeight,
                    child: Container(
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        borderRadius:
                            BorderRadius.circular(constraints.maxHeight / 2),
                        color: Colors.black38,
                      ),
                      child: CircularProgressIndicator(),
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.center,
                  child: SizedBox(
                    width: constraints.maxHeight,
                    height: constraints.maxHeight,
                    child: ClipRRect(
                      borderRadius:
                          BorderRadius.circular(constraints.maxHeight / 2),
                      child: IgnorePointer(
                        ignoring: _disablePhoto,
                        child: ImagePickerWidget(
                          onImageSelected: _onImageSelected,
                          ratioX: 1,
                          ratioY: 1,
                          circleShape: true,
                          child: child,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            );
          }),
        );
      },
    );
  }

  @override
  void dispose() {
    _updatePhotoResultSubscription.cancel();
    super.dispose();
  }
}
