import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:bottles/blocs/blocs.dart';
import 'package:bottles/bottles_localizations.dart';
import 'package:bottles/helpers/no_overscroll_behaviour.dart';
import 'package:bottles/screens/profile/user_info_form.dart';
import 'package:bottles/screens/profile/change_password_form.dart';
import 'package:bottles/screens/profile/avatar_form.dart';

final double _kAvatarRadius = 60.0;

class ProfileScreen extends StatefulWidget {
  static PageRoute buildPageRoute() {
    if (Platform.isIOS) {
      return CupertinoPageRoute(builder: _builder);
    }

    return MaterialPageRoute(builder: _builder);
  }

  static Widget _builder(BuildContext context) {
    final usersBloc = Provider.of<UsersBloc>(context);

    return ProfileScreen(
      usersBloc: usersBloc,
    );
  }

  final UsersBloc usersBloc;

  ProfileScreen({
    Key key,
    @required this.usersBloc,
  })  : assert(usersBloc != null),
        super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  BottlesLocalizations get localizations => BottlesLocalizations.of(context);

  @override
  Widget build(BuildContext context) {
    final primaryColor = Theme.of(context).primaryColor;

    return Scaffold(
      backgroundColor: primaryColor,
      appBar: AppBar(
        title: Text(localizations.profile),
        centerTitle: true,
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
        },
        child: Container(
          color: Colors.grey[100],
          child: Stack(
            children: [
              SafeArea(
                child: _buildBody(),
              ),
              AvatarForm(usersBloc: widget.usersBloc),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildBody() {
    return ScrollConfiguration(
      behavior: NoOverScrollBehavior(),
      child: Theme(
        data: Theme.of(context).copyWith(
          inputDecorationTheme: InputDecorationTheme(
            border: InputBorder.none,
            contentPadding: EdgeInsets.all(10.0),
          ),
        ),
        child: ListView(
          physics: ClampingScrollPhysics(),
          padding: EdgeInsets.all(20.0).copyWith(top: 20.0 + _kAvatarRadius),
          children: [
            SizedBox(
              height: _kAvatarRadius,
            ),
            UserInfoForm(usersBloc: widget.usersBloc),
            Divider(height: 32.0),
            ChangePasswordForm(usersBloc: widget.usersBloc),
          ],
        ),
      ),
    );
  }
}
