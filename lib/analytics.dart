import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/cupertino.dart';

abstract class IAnalytics {
  NavigatorObserver get observer;

  Future<void> sendLogin(String method);
}

class Analytics implements IAnalytics {
  final NavigatorObserver _observer;
  final FirebaseAnalytics _firebaseAnalytics;

  Analytics({
    FirebaseAnalytics firebaseAnalytics,
  })  : assert(firebaseAnalytics != null),
        _firebaseAnalytics = firebaseAnalytics,
        _observer = FirebaseAnalyticsObserver(analytics: firebaseAnalytics);

  NavigatorObserver get observer => _observer;

  @override
  Future<void> sendLogin(String method) {
    return _firebaseAnalytics.logLogin(loginMethod: method);
  }
}
