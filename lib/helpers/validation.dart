import 'package:flutter/widgets.dart';

import 'package:bottles/bottles_localizations.dart';

typedef ContextFormFieldValidator<T> = String Function(BuildContext context, T value);

String validate<T>(BuildContext context, T value, List<ContextFormFieldValidator<T>> validators) {
  String errorMessage;

  for (ContextFormFieldValidator<T> validator in validators) {
    errorMessage = validator(context, value);
    if (errorMessage != null) break;
  }

  return errorMessage;
}

String requiredField<T>(BuildContext context, T value) {
  if (value == null || (value is String && value.isEmpty)) {
    return BottlesLocalizations.of(context).required;
  }

  return null;
}

String isValidEmail(BuildContext context, String value) {
  final emailRegex = RegExp(r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$");
  if (!emailRegex.hasMatch(value)) {
    return BottlesLocalizations.of(context).invalidEmail;
  }

  return null;
}

ContextFormFieldValidator<String> minLength(int length) {
  return (context, value) {
    if (value.length < length) {
      return BottlesLocalizations.of(context).passwordLengthError(length);
    }

    return null;
  };
}

ContextFormFieldValidator<String> mustBeEqualTo(String anotherValue, String fieldName) {
  return (context, value) {
    if (value != anotherValue) {
      return BottlesLocalizations.of(context).mustBeEqualTo(fieldName);
    }

    return null;
  };
}