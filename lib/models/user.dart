import 'package:meta/meta.dart';
import 'package:firebase_auth/firebase_auth.dart';

class User {
  final String _uid;
  final String _email;
  final String _displayName;
  final String _photoUrl;

  User({
    @required String uid,
    @required String email,
    String displayName,
    String photoUrl,
  })  : assert(uid != null),
        assert(email != null),
        _uid = uid,
        _email = email,
        _displayName = displayName,
        _photoUrl = photoUrl;

  String get uid => _uid;

  String get email => _email;

  String get displayName => _displayName;

  String get photoUrl => _photoUrl;

  bool operator ==(other) {
    return uid == other.uid;
  }

  @override
  String toString() {
    return '{ uid: $_uid, email: $_email, displayName: $_displayName, photoUrl: $_photoUrl, }';
  }
}
