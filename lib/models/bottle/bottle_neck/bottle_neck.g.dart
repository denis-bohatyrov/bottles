// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bottle_neck.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BottleNeck _$BottleNeckFromJson(Map<String, dynamic> json) {
  return BottleNeck(
    uid: json['uid'] as String,
    name: json['name'] as String,
    imageUrl: json['imageUrl'] as String,
  );
}

Map<String, dynamic> _$BottleNeckToJson(BottleNeck instance) =>
    <String, dynamic>{
      'uid': instance.uid,
      'name': instance.name,
      'imageUrl': instance.imageUrl,
    };
