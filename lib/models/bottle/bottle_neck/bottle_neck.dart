import 'package:json_annotation/json_annotation.dart';

part 'bottle_neck.g.dart';

@JsonSerializable()
class BottleNeck {
  String uid;
  String name;
  String imageUrl;

  BottleNeck({
    this.uid,
    this.name,
    this.imageUrl,
  });

  factory BottleNeck.fromJson(Map<String, dynamic> json) =>
      _$BottleNeckFromJson(json);

  Map<String, dynamic> toJson() => _$BottleNeckToJson(this);
}
