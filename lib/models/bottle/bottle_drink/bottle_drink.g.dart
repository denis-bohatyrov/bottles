// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bottle_drink.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BottleDrink _$BottleDrinkFromJson(Map<String, dynamic> json) {
  return BottleDrink(
    uid: json['uid'] as String,
    name: json['name'] as String,
  );
}

Map<String, dynamic> _$BottleDrinkToJson(BottleDrink instance) =>
    <String, dynamic>{
      'uid': instance.uid,
      'name': instance.name,
    };
