import 'package:json_annotation/json_annotation.dart';

part 'bottle_drink.g.dart';

@JsonSerializable()
class BottleDrink {
  String uid;
  String name;

  BottleDrink({
    this.uid,
    this.name,
  });

  factory BottleDrink.fromJson(Map<String, dynamic> json) => _$BottleDrinkFromJson(json);

  Map<String, dynamic> toJson() => _$BottleDrinkToJson(this);
}
