// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bottle_material.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BottleMaterial _$BottleMaterialFromJson(Map<String, dynamic> json) {
  return BottleMaterial(
    uid: json['uid'] as String,
    name: json['name'] as String,
  );
}

Map<String, dynamic> _$BottleMaterialToJson(BottleMaterial instance) =>
    <String, dynamic>{
      'uid': instance.uid,
      'name': instance.name,
    };
