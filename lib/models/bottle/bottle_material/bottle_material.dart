import 'package:json_annotation/json_annotation.dart';

part 'bottle_material.g.dart';

@JsonSerializable()
class BottleMaterial {
  String uid;
  String name;

  BottleMaterial({
    this.uid,
    this.name,
  });

  factory BottleMaterial.fromJson(Map<String, dynamic> json) =>
      _$BottleMaterialFromJson(json);

  Map<String, dynamic> toJson() => _$BottleMaterialToJson(this);
}
