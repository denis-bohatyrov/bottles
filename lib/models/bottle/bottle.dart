import 'dart:convert';
import 'package:json_annotation/json_annotation.dart';

import 'package:bottles/models/bottle/bottle_neck/bottle_neck.dart';
import 'package:bottles/models/bottle/bottle_body/bottle_body.dart';
import 'package:bottles/models/bottle/bottle_bottom/bottle_bottom.dart';
import 'package:bottles/models/bottle/bottle_brand/bottle_brand.dart';
import 'package:bottles/models/bottle/bottle_color/bottle_color.dart';
import 'package:bottles/models/bottle/bottle_material/bottle_material.dart';
import 'package:bottles/models/bottle/bottle_drink/bottle_drink.dart';

part 'bottle.g.dart';

@JsonSerializable()
class Bottle {
  String uid;
  String name;
  double price;
  bool hasScrewThread; // резьба на горлышке,
  bool isEngraved; // гравировка,
  BottleNeck neck; // фотрма горлыщка
  BottleBody body; // форма основного тела,
  BottleBottom bottom; // форма дна
  BottleBrand brand; // бренд
  BottleColor color; // цвет
  BottleMaterial material; // материал бутылки
  BottleDrink drink; // напитка
  List<String> images;

  Bottle({
    this.uid,
    this.name,
    this.price,
    this.hasScrewThread,
    this.isEngraved,
    this.neck,
    this.body,
    this.bottom,
    this.brand,
    this.color,
    this.material,
    this.drink,
    this.images,
  });

  factory Bottle.fromJson(Map<String, dynamic> json) =>
      _$BottleFromJson(json);

  Map<String, dynamic> toJson() => _$BottleToJson(this);

  @override
  String toString() {
    return jsonEncode(toJson());
  }
}
