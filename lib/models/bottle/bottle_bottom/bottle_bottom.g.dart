// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bottle_bottom.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BottleBottom _$BottleBottomFromJson(Map<String, dynamic> json) {
  return BottleBottom(
    uid: json['uid'] as String,
    name: json['name'] as String,
    imageUrl: json['imageUrl'] as String,
  );
}

Map<String, dynamic> _$BottleBottomToJson(BottleBottom instance) =>
    <String, dynamic>{
      'uid': instance.uid,
      'name': instance.name,
      'imageUrl': instance.imageUrl,
    };
