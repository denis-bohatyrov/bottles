import 'package:json_annotation/json_annotation.dart';

part 'bottle_bottom.g.dart';

@JsonSerializable()
class BottleBottom {
  String uid;
  String name;
  String imageUrl;

  BottleBottom({this.uid, this.name, this.imageUrl,});

  factory BottleBottom.fromJson(Map<String, dynamic> json) =>
      _$BottleBottomFromJson(json);

  Map<String, dynamic> toJson() => _$BottleBottomToJson(this);
}
