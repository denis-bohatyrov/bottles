// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bottle_body.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BottleBody _$BottleBodyFromJson(Map<String, dynamic> json) {
  return BottleBody(
    uid: json['uid'] as String,
    name: json['name'] as String,
    imageUrl: json['imageUrl'] as String,
  );
}

Map<String, dynamic> _$BottleBodyToJson(BottleBody instance) =>
    <String, dynamic>{
      'uid': instance.uid,
      'name': instance.name,
      'imageUrl': instance.imageUrl,
    };
