import 'package:json_annotation/json_annotation.dart';

part 'bottle_body.g.dart';

@JsonSerializable()
class BottleBody {
  String uid;
  String name;
  String imageUrl;

  BottleBody({
    this.uid,
    this.name,
    this.imageUrl,
  });

  factory BottleBody.fromJson(Map<String, dynamic> json) =>
      _$BottleBodyFromJson(json);

  Map<String, dynamic> toJson() => _$BottleBodyToJson(this);
}
