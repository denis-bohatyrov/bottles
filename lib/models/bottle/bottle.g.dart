// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bottle.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Bottle _$BottleFromJson(Map<String, dynamic> json) {
  return Bottle(
    uid: json['uid'] as String,
    name: json['name'] as String,
    price: (json['price'] as num)?.toDouble(),
    hasScrewThread: json['hasScrewThread'] as bool,
    isEngraved: json['isEngraved'] as bool,
    neck: json['neck'] == null
        ? null
        : BottleNeck.fromJson(json['neck'] as Map<String, dynamic>),
    body: json['body'] == null
        ? null
        : BottleBody.fromJson(json['body'] as Map<String, dynamic>),
    bottom: json['bottom'] == null
        ? null
        : BottleBottom.fromJson(json['bottom'] as Map<String, dynamic>),
    brand: json['brand'] == null
        ? null
        : BottleBrand.fromJson(json['brand'] as Map<String, dynamic>),
    color: json['color'] == null
        ? null
        : BottleColor.fromJson(json['color'] as Map<String, dynamic>),
    material: json['material'] == null
        ? null
        : BottleMaterial.fromJson(json['material'] as Map<String, dynamic>),
    drink: json['drink'] == null
        ? null
        : BottleDrink.fromJson(json['drink'] as Map<String, dynamic>),
    images: (json['images'] as List)?.map((e) => e as String)?.toList(),
  );
}

Map<String, dynamic> _$BottleToJson(Bottle instance) => <String, dynamic>{
      'uid': instance.uid,
      'name': instance.name,
      'price': instance.price,
      'hasScrewThread': instance.hasScrewThread,
      'isEngraved': instance.isEngraved,
      'neck': instance.neck,
      'body': instance.body,
      'bottom': instance.bottom,
      'brand': instance.brand,
      'color': instance.color,
      'material': instance.material,
      'drink': instance.drink,
      'images': instance.images,
    };
