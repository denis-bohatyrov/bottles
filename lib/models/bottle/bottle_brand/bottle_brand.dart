import 'package:json_annotation/json_annotation.dart';

part 'bottle_brand.g.dart';

@JsonSerializable()
class BottleBrand {
  String uid;
  String name;

  BottleBrand({
    this.uid,
    this.name,
  });

  factory BottleBrand.fromJson(Map<String, dynamic> json) =>
      _$BottleBrandFromJson(json);

  Map<String, dynamic> toJson() => _$BottleBrandToJson(this);
}
