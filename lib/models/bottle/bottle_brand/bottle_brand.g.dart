// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bottle_brand.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BottleBrand _$BottleBrandFromJson(Map<String, dynamic> json) {
  return BottleBrand(
    uid: json['uid'] as String,
    name: json['name'] as String,
  );
}

Map<String, dynamic> _$BottleBrandToJson(BottleBrand instance) =>
    <String, dynamic>{
      'uid': instance.uid,
      'name': instance.name,
    };
