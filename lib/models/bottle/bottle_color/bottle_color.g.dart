// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bottle_color.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BottleColor _$BottleColorFromJson(Map<String, dynamic> json) {
  return BottleColor(
    uid: json['uid'] as String,
    name: json['name'] as String,
    hex: json['hex'] as String,
  );
}

Map<String, dynamic> _$BottleColorToJson(BottleColor instance) =>
    <String, dynamic>{
      'uid': instance.uid,
      'name': instance.name,
      'hex': instance.hex,
    };
