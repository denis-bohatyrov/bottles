import 'package:json_annotation/json_annotation.dart';

part 'bottle_color.g.dart';

@JsonSerializable()
class BottleColor {
  String uid;
  String name;
  String hex;

  BottleColor({
    this.uid,
    this.name,
    this.hex,
  });

  factory BottleColor.fromJson(Map<String, dynamic> json) =>
      _$BottleColorFromJson(json);

  Map<String, dynamic> toJson() => _$BottleColorToJson(this);
}
