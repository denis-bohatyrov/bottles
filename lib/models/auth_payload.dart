abstract class AuthPayload {}

class GoogleLoginPayload implements AuthPayload {}

class EmailPasswordPayload implements AuthPayload {
  String email;
  String password;

  EmailPasswordPayload({
     this.email,
     this.password
  }) ;

  @override
  String toString() {
    return '{ email: $email, password: $password }';
  }
}