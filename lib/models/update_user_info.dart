class UpdateUserInfo {
  String fullName;
  String email;

  UpdateUserInfo({
    this.fullName,
    this.email,
  });
}
