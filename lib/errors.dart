class WrongCredentialsException implements Exception {
  @override
  String toString() => 'User with provided credentials was not found';
}

class TooManyRequestsException implements Exception {
  @override
  String toString() => 'There was too many attempts to sign in as this user';
}

class GoogleSignInCanceledException implements Exception {
  String toString() => 'Google sign in was cancelled, this error just strange behaviour of google signin';
}

class EmailAlreadyInUseException implements Exception {
  @override
  String toString() => 'User with provided email already exists';
}

class RequiresRecentLoginException implements Exception {
  @override
  String toString() => 'This operation is sensitive and requires recent authentication. Log in again before retrying this request';
}