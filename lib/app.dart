import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';

import 'package:bottles/analytics.dart';
import 'package:bottles/api/bottles_api.dart';
import 'package:bottles/api/auth_api.dart';
import 'package:bottles/api/users_api.dart';
import 'package:bottles/blocs/blocs.dart';
import 'package:bottles/bottles_localizations.dart';
import 'package:bottles/screens/screens.dart';

FirebaseAnalytics _analytics = FirebaseAnalytics();

class BottlesApp extends StatefulWidget {
  final AuthApi authApi;
  final bool isSignedIn;

  BottlesApp({
    Key key,
    @required this.authApi,
    @required this.isSignedIn,
  })  : assert(authApi != null),
        assert(isSignedIn != null),
        super(key: key);

  @override
  _BottlesAppState createState() => _BottlesAppState();
}

class _BottlesAppState extends State<BottlesApp> {
  AuthBloc _authBloc;
  UsersBloc _usersBloc;
  BottlesBloc _bottlesBloc;

  @override
  void initState() {
    _authBloc = AuthBloc(
      authApi: widget.authApi,
      analytics: Analytics(firebaseAnalytics: _analytics),
    );
    _usersBloc = UsersBloc(usersApi: UsersApi());
    _bottlesBloc = BottlesBloc(bottlesApi: BottlesApi());
    super.initState();
  }

  ThemeData get _theme => ThemeData(
        primarySwatch: Colors.red,
        scaffoldBackgroundColor: Colors.white,
        inputDecorationTheme: InputDecorationTheme(
          border: _inputBorder,
        ),
        disabledColor: Colors.grey,
        dividerColor: Colors.transparent,
        appBarTheme: AppBarTheme(
          elevation: 0.0,
        ),
        buttonTheme: ButtonThemeData(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(50),
          ),
        ),
      );

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        BlocProvider(bloc: _authBloc),
        BlocProvider(bloc: _usersBloc),
        BlocProvider(bloc: _bottlesBloc),
      ],
      child: MaterialApp(
        localizationsDelegates: [
          BottleLocalizationsDelegate(),
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate
        ],
        navigatorObservers: [
          FirebaseAnalyticsObserver(analytics: _analytics),
        ],
        supportedLocales: [
          Locale('en'),
          Locale('ru'),
        ],
        onGenerateTitle: (context) => BottlesLocalizations.of(context).appName,
        theme: _theme,
        home: widget.isSignedIn ? _buildHomeScreen() : _buildLoginScreen(),
      ),
    );
  }

  Widget _buildHomeScreen() {
    return HomeScreen(
      bottlesBloc: _bottlesBloc,
    );
  }

  LoginScreen _buildLoginScreen() {
    return LoginScreen(
      authBloc: _authBloc,
    );
  }
}

final _inputBorder = OutlineInputBorder(
  borderRadius: BorderRadius.circular(50),
);
