import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/services.dart';

import 'package:bottles/firebase_utils/firebase_user_to_user.dart';
import 'package:bottles/models/models.dart';
import 'package:bottles/errors.dart';

final _auth = FirebaseAuth.instance;
final _storage = FirebaseStorage.instance;

abstract class IUsersApi {
  Future<User> currentUser();

  Stream<User> currentUserStream();

  Future<void> updateEmail(String email);

  Future<void> updateName(String name);

  Future<void> changePassword(String password);

  Future<void> updateProfilePhoto(File image);
}

class UsersApi implements IUsersApi {
  @override
  Future<User> currentUser() async {
     final userInfo = await _auth.currentUser();
     if (userInfo != null) {
       return userInfoToUserAdapter(userInfo);
     }

     return null;
  }

  @override
  Stream<User> currentUserStream() {
    return _auth.onAuthStateChanged.map(userInfoToUserAdapter);
  }

  @override
  Future<void> updateEmail(String email) async {
    final user = await _auth.currentUser();
    try {
      return await user.updateEmail(email);
    } on PlatformException catch (error) {
      if (error.code == 'ERROR_EMAIL_ALREADY_IN_USE') {
        throw EmailAlreadyInUseException();
      } else if (error.code == 'ERROR_REQUIRES_RECENT_LOGIN') {
        throw RequiresRecentLoginException();
      }

      print('Update Email Error: $error');
      rethrow;
    }
  }

  @override
  Future<void> updateName(String name) async {
    final user = await _auth.currentUser();
    final userInfo = UserUpdateInfo()..displayName = name;

    return user.updateProfile(userInfo);
  }

  @override
  Future<void> changePassword(String password) async {
    try {
      final user = await _auth.currentUser();
      return await user.updatePassword(password);
    } on PlatformException catch (error) {
      if (error.code == 'ERROR_REQUIRES_RECENT_LOGIN') {
        throw RequiresRecentLoginException();
      }

      print('Change Password Error: $error');
      rethrow;
    }
  }

  @override
  Future<void> updateProfilePhoto(File image) async {
    final user = await _auth.currentUser();

    final snapshot = await _storage
        .ref()
        .child('images')
        .child('avatars')
        .child(user.uid)
        .putFile(image)
        .onComplete;

    final url = await snapshot.ref.getDownloadURL();
    final UserUpdateInfo updateInfo = UserUpdateInfo()..photoUrl = url;
    await user.updateProfile(updateInfo);
  }
}
