import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:bottles/firebase_utils/deep_get_references.dart';
import 'package:bottles/models/models.dart';

final _firestore = Firestore.instance;

abstract class IBottlesApi {
  Stream<List<Bottle>> get bottles;
}

class BottlesApi implements IBottlesApi {
  @override
  Stream<List<Bottle>> get bottles {
    return _firestore
        .collection('bottles')
        .snapshots()
        .asyncMap((snapshot) async {
      List<DocumentSnapshot> documents = List.from(snapshot.documents);
      List<Bottle> bottles = [];

      for (DocumentSnapshot document in documents) {
        bottles.add(
          Bottle.fromJson(
            await deepGetReferences(document),
          ),
        );
      }
      return bottles;
    });
  }
}
