import 'package:flutter/services.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

import 'package:bottles/firebase_utils/firebase_user_to_user.dart';
import 'package:bottles/errors.dart';
import 'package:bottles/models/models.dart';

final GoogleSignIn _googleSignIn = GoogleSignIn();
final FirebaseAuth _auth = FirebaseAuth.instance;

const _wrongCredentialCodes = [
  'ERROR_INVALID_EMAIL',
  'ERROR_WRONG_PASSWORD',
  'ERROR_USER_NOT_FOUND',
  'ERROR_USER_DISABLED',
];

abstract class IAuthApi {
  Future<User> loginWithEmail(EmailPasswordPayload payload);

  Future<User> loginOrRegisterWithGoogle();

  Future<User> registerWithEmail(EmailPasswordPayload payload);

  Future<void> resetPassword(String email);

  Future<void> signOut();

  Future<bool> isSignedIn();

  Future<bool> checkSignedInAndRefresh();
}

class AuthApi implements IAuthApi {
  @override
  Future<User> loginWithEmail(EmailPasswordPayload payload) async {
    try {
      UserInfo userInfo = await _auth.signInWithEmailAndPassword(
        email: payload.email,
        password: payload.password,
      );

      return userInfoToUserAdapter(userInfo);
    } on PlatformException catch (error) {
      if (_wrongCredentialCodes.contains(error.code)) {
        throw WrongCredentialsException();
      }

      if (error.code == 'ERROR_TOO_MANY_REQUESTS') {
        throw TooManyRequestsException();
      }

      print('Email Sign In Error: $error');
      rethrow;
    }
  }

  @override
  Future<User> loginOrRegisterWithGoogle() async {
    try {
      final isSignedIn = await _googleSignIn.isSignedIn();

      if (isSignedIn) {
        await _googleSignIn.signOut();
      }

      final GoogleSignInAccount googleUser = await _googleSignIn.signIn();

      if (googleUser == null) {
        throw GoogleSignInCanceledException();
      }

      final GoogleSignInAuthentication googleAuth =
          await googleUser.authentication;

      final AuthCredential credential = GoogleAuthProvider.getCredential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );

      final UserInfo userInfo = await _auth.signInWithCredential(credential);

      return userInfoToUserAdapter(userInfo);
    } on PlatformException catch (error) {
      print(error);
      if (_wrongCredentialCodes.contains(error.code)) {
        throw WrongCredentialsException();
      }

      if (error.code == 'ERROR_TOO_MANY_REQUESTS') {
        throw TooManyRequestsException();
      }

      print('Google Sign In Error: $error');
      rethrow;
    }
  }

  @override
  Future<User> registerWithEmail(EmailPasswordPayload payload) async {
    try {
      final UserInfo userInfo = await _auth.createUserWithEmailAndPassword(
        email: payload.email,
        password: payload.password,
      );

      return userInfoToUserAdapter(userInfo);
    } on PlatformException catch (error) {
      if (error.code == 'ERROR_EMAIL_ALREADY_IN_USE') {
        throw EmailAlreadyInUseException();
      }

      print('Email Register Error: $error');
      rethrow;
    }
  }

  @override
  Future<void> resetPassword(String email) async {
    try {
      await _auth.sendPasswordResetEmail(email: email);
    } on PlatformException catch (error) {
      if (error.code == 'ERROR_USER_NOT_FOUND') {
        throw WrongCredentialsException();
      }

      print('Send Reset Password Email Error: $error');
      rethrow;
    }
  }

  @override
  Future<void> signOut() async {
    await _auth.signOut();
  }

  @override
  Future<bool> isSignedIn() async {
    UserInfo user = await _auth.currentUser();
    return user != null;
  }

  @override
  Future<bool> checkSignedInAndRefresh() async {
    FirebaseUser user = await _auth.currentUser();
    if (user == null) {
      return false;
    }

    await user.getIdToken();
    await user.reload();
    return true;
  }
}
