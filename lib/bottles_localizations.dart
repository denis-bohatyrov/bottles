import 'dart:async';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'l10n/messages_all.dart';

class BottlesLocalizations {
  static Future<BottlesLocalizations> load(Locale locale) {
    final String name =
        locale.countryCode == null ? locale.languageCode : locale.toString();
    final String localeName = Intl.canonicalizedLocale(name);

    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      return BottlesLocalizations();
    });
  }

  static BottlesLocalizations of(BuildContext context) {
    return Localizations.of<BottlesLocalizations>(
        context, BottlesLocalizations);
  }

  String get appName {
    return Intl.message(
      'Bottles',
      name: 'appName',
    );
  }

  String get email {
    return Intl.message(
      'Email',
      name: 'email',
    );
  }

  String get password {
    return Intl.message(
      'Password',
      name: 'password',
    );
  }

  String get confirmPassword {
    return Intl.message(
      'Confirm password',
      name: 'confirmPassword',
    );
  }

  String get login {
    return Intl.message(
      'Login',
      name: 'login',
    );
  }

  String get forgotPassword {
    return Intl.message(
      'Forgot password?',
      name: 'forgotPassword',
    );
  }

  String get dontHaveAccount {
    return Intl.message(
      '''Don't have an account?''',
      name: 'dontHaveAccount',
    );
  }

  String get signUpVerb {
    return Intl.message(
      'Sign up',
      name: 'signUpVerb',
    );
  }

  String get signUpNoun {
    return Intl.message(
      'Sign up',
      name: 'signUpNoun',
    );
  }

  String get restorePassword {
    return Intl.message(
      'Restore password',
      name: 'restorePassword',
    );
  }

  String get restore {
    return Intl.message(
      'Restore',
      name: 'restore',
    );
  }

  String get required {
    return Intl.message(
      'Required field',
      name: 'required',
    );
  }

  String get invalidEmail {
    return Intl.message(
      'Invalid email',
      name: 'invalidEmail',
    );
  }

  String passwordLengthError(int length) {
    return Intl.plural(
      length,
      one: 'Password length must be greater that $length character',
      other: 'Password length must be greater that $length characters',
      name: 'passwordLengthError',
      args: [length],
    );
  }

  String mustBeEqualTo(String value) {
    return Intl.message(
      'Must be equal to $value',
      name: 'mustBeEqualTo',
      args: [value],
    );
  }

  String get unknownError {
    return Intl.message(
      'Unknown error',
      name: 'unknownError',
    );
  }

  String get wrongCredentials {
    return Intl.message(
      'Wrong email or password',
      name: 'wrongCredentials',
    );
  }

  String get tooManyRequests {
    return Intl.message(
      'Too many requests, chill bro',
      name: 'tooManyRequests',
    );
  }

  String get emailInUse {
    return Intl.message(
      'Email already in use',
      name: 'emailInUse',
    );
  }

  String get accountWithEmailNotFound {
    return Intl.message(
      'Account with provided email not found',
      name: 'accountWithEmailNotFound',
    );
  }

  String get restoreEmailSent {
    return Intl.message(
      'Restore link was sent to your email',
      name: 'restoreEmailSent',
    );
  }

  String get signOut {
    return Intl.message(
      'Sign out',
      name: 'signOut',
    );
  }

  String get profile {
    return Intl.message(
      'Profile',
      name: 'profile',
    );
  }

  String get save {
    return Intl.message(
      'Save',
      name: 'save',
    );
  }

  String get fullName {
    return Intl.message(
      'Full name',
      name: 'fullName',
    );
  }

  String get changePassword {
    return Intl.message(
      'Change password',
      name: 'changePassword',
    );
  }

  String get userInfo {
    return Intl.message(
      'User info',
      name: 'userInfo',
    );
  }

  String get reLoginException {
    return Intl.message(
      'Please log in again to perform this operation',
      name: 'reLoginException',
    );
  }

  String get passwordChangeSuccess {
    return Intl.message(
      'Password was successfully changed',
      name: 'passwordChangeSuccess',
    );
  }

  String get takeAPhoto {
    return Intl.message(
      'Take a photo',
      name: 'takeAPhoto',
    );
  }

  String get selectFromGallery {
    return Intl.message(
      'Select from gallery',
      name: 'selectFromGallery',
    );
  }
}

class BottleLocalizationsDelegate
    extends LocalizationsDelegate<BottlesLocalizations> {
  const BottleLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) {
    return ['en', 'ru'].contains(locale.languageCode);
  }

  @override
  Future<BottlesLocalizations> load(Locale locale) {
    return BottlesLocalizations.load(locale);
  }

  @override
  bool shouldReload(LocalizationsDelegate<BottlesLocalizations> old) {
    return false;
  }
}
