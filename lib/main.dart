import 'package:flutter/material.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';

import 'package:bottles/app.dart';
import 'package:bottles/api/auth_api.dart';


Future<void> main() async {
  FlutterError.onError = Crashlytics.instance.recordFlutterError;

  final authApi = AuthApi();
  final isSignedIn = await authApi.checkSignedInAndRefresh();

  runApp(
    BottlesApp(
      authApi: authApi,
      isSignedIn: isSignedIn,
    ),
  );
}
