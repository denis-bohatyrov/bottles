import 'dart:async';

import 'package:bottles/analytics.dart';
import 'package:bottles/errors.dart';
import 'package:bottles/models/auth_payload.dart';
import 'package:bottles/models/models.dart';
import 'package:test/test.dart';

import 'package:bottles/api/auth_api.dart';
import 'package:bottles/blocs/auth_bloc.dart';
import '../mocks/mocks.dart';

void main() {
  group('Login', () {
    AuthBloc authBloc;

    setUp(() {
      IAuthApi authApi = AuthApiMock();
      IAnalytics analytics = AnalyticsMock();
      authBloc = AuthBloc(
        authApi: authApi,
        analytics: analytics,
      );
    });

    test('with wrong email', () {
      expect(
        authBloc.loginResultStream,
        emitsError(TypeMatcher<WrongCredentialsException>()),
      );

      authBloc.loginWithEmailSink.add(
        EmailPasswordPayload(email: 'asd@asd.asd', password: '123456'),
      );
    });

    test('with wrong password', () {
      expect(
        authBloc.loginResultStream,
        emitsError(TypeMatcher<WrongCredentialsException>()),
      );

      authBloc.loginWithEmailSink.add(
        EmailPasswordPayload(email: 'test@test.com', password: '000'),
      );
    });

    test('with right credentials', () {
      expect(
        authBloc.loginResultStream,
        emits(User(uid: '1', email: 'test@test.com')),
      );

      authBloc.loginWithEmailSink.add(
        EmailPasswordPayload(email: 'test@test.com', password: '123456'),
      );
    });

    tearDown(() {
      authBloc.dispose();
    });
  });
}
