import 'package:bottles/errors.dart';

import 'package:bottles/api/auth_api.dart';
import 'package:bottles/models/models.dart';

final _mockUser = User(uid: '1', email: 'test@test.com',);

class AuthApiMock implements IAuthApi {
  @override
  Future<bool> checkSignedInAndRefresh() => Future.value(true);

  @override
  Future<bool> isSignedIn() => Future.value(false);

  @override
  Future<User> loginOrRegisterWithGoogle() {
    // TODO: implement loginOrRegisterWithGoogle
    return null;
  }

  @override
  Future<User> loginWithEmail(EmailPasswordPayload payload) {
    if (payload.email != _mockUser.email || payload.password != '123456') {
      throw WrongCredentialsException();
    }

    return Future.value(_mockUser);
  }

  @override
  Future<User> registerWithEmail(EmailPasswordPayload payload) {
    if (payload.email == _mockUser.email) {
      throw EmailAlreadyInUseException();
    }

    return Future.value(User(uid: '2', email: payload.email));
  }

  @override
  Future<void> resetPassword(String email) {
    if (email != _mockUser.email) {
      throw WrongCredentialsException();
    }

    return null;
  }

  @override
  Future<void> signOut() {
    return Future.value();
  }

}