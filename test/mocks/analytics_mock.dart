import 'package:bottles/analytics.dart';
import 'package:flutter/src/widgets/navigator.dart';

class AnalyticsMock implements IAnalytics {
  @override
  NavigatorObserver get observer => null;

  @override
  Future<void> sendLogin(String method) async {
    return null;
  }
}